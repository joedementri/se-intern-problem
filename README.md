# Phillies Intern Problem

In this repository, you will be presented with a very small application with a single page that contains a parking request form. This form was taken from a real Phillies app and edited to create this problem.

The purpose of this problem is to evaluate a candidate's ability to solve problems that a future Phillies Software Engineer intern would face.

There will be two issues with the code that we will ask you to fix.

1. In the Parking Request form, there are inputs for first/last name, organization, and requested parking lot. When the form is submitted, the last name does not show up in the "success" dialog. Find out why and then add the necessary code to make it so that the last name shows up in the dialog after form submission.
2. The success dialog's button is not displaying properly. It says "check" when it should be displaying a check-mark icon. Add the necessary line to the `index.html` page to get the icon to display properly. Hint: Use online documentation

There will only be two deliverables:

1. Your source code pushed to a private Gitlab repository and shared only with jkemmerer [at] phillies.com
2. A screenshot of the parking request form. The form must be submitted with your name.

## Dependencies

1. Node.Js v8.x or higher and NPM version 5.x or higher (`https://angular.io/guide/quickstart`)
1. Latest Chrome or Safari browser

## Downloading/Installing the App

1. Clone the app
2. Run `npm install`

## Running the App

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build. Use the `--build-optimizer` flag for disabling the vendor chunk and enabling full tree-shaking.
