import { TestBed, inject } from '@angular/core/testing';

import { ParkingRequestService } from './parking-request.service';

describe('ParkingRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParkingRequestService]
    });
  });

  it('should be created', inject([ParkingRequestService], (service: ParkingRequestService) => {
    expect(service).toBeTruthy();
  }));
});
