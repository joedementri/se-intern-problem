import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleOkModal } from './simple-ok.component';

describe('SimpleOkComponent', () => {
  let component: SimpleOkModal;
  let fixture: ComponentFixture<SimpleOkModal>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [SimpleOkModal]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleOkModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
